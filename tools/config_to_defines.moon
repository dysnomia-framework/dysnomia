config = require 'dysnomia.config'
string = require 'string'
math = require 'math'

walk=(tbl,pre)->
  for k,v in pairs tbl
    if k == '_a'
      print '#define AL_' .. pre ..' '..#tbl
      continue
    name = pre .. '_' .. string.upper k
    print '#define E_' .. name
    print '#define E10_' .. name .. ' 1'
    print '#define EOO_' .. name .. ' on'
    print '#define ETF_' .. name .. ' true'
    switch type(v)
      when 'table'
        walk v, name
      when 'string'
        print '#define S_' .. name .. ' ' .. tostring v
      when 'number'
        print '#define F_'..name..' '..tostring v
        print '#define I_'..name..' '..tostring math.floor v+0.5
      when 'boolean'
        print '#define B_' .. name if v
        print '#define B10_' .. name .. ' ' .. (v and 1 or 0)
        print '#define BOO_' .. name .. ' ' .. (v and "on" or "off")
        print '#define BTF_' .. name .. ' ' .. (v and "true" or "false")

walk(config.ngx,'CONFIG')
