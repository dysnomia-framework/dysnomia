import extend from require 'moon'
import expnil,isexpnil from require "dysnomia.utils.expnil"
import insert,remove from require 'table'

dsl=require 'dysnomia.utils.dsl'
defaults = require 'dysnomia.config.defaults'

dsltree= dsl (require 'config'), {NIL:expnil}

isArray=(tbl)->
  numKeys,numIndices = 0,0
  numKeys +=1 for _, _ in pairs(tbl)
  numIndices +=1 for _, _ in ipairs(tbl)
  return numKeys == numIndices

walktree=(tree,tbl={})->
  for e in *tree
    tbl[e._n]={}
    for a in *(e._a)
      insert tbl[e._n], if type(a)=="table" and a._f
        walktree a,{}
      else
        a
    if type(tbl[e._n][1])=='table' and tbl[e._n][1]._arr
      remove tbl[e._n], 1
      tbl[e._n]._a=true
    elseif #(tbl[e._n])==1
      tbl[e._n]=tbl[e._n][1]
  tbl

config = walktree dsltree

filldefaults=(cfg,def)->
  for k,v in pairs def
    if type(v)=='table' and isArray(v)
      v._a=true
    if isexpnil(cfg[k])
      continue
    elseif type(v)=='table' and (type(cfg[k])=='table' or type(cfg[k])=='nil')
      if type(cfg[k])=='nil'
        cfg[k]={}
      filldefaults cfg[k], v
    elseif type(cfg[k])=='nil'
      cfg[k]=v

filldefaults config, defaults

delexpnils=(tbl)->
  for k,v in pairs tbl
    if isexpnil(v)
      tbl[k]=nil
    elseif type(v)=='table'
      delexpnils(v)

delexpnils config

config
