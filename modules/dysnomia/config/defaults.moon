{
  routers: {
    regex_router: {
      debug: false
    }
  }
  ngx: {
    debug: false
    daemon: true
    worker_processes: 10
    events: {
      worker_connections: 1024
    }
    http: {
      server: {
        port: 80
        lua_code_cache: true
        request_proxy: {
          resolver: '8.8.8.8'
          version: '1.1'
          location: '/_dysnomia/request_proxy'
          uri_variable: '_dysnomia_request_proxy_uri'
        }
      }
    }
  }
}
