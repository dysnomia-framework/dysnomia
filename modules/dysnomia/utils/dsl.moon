import run_with_scope, defaultbl, mixin_table from require "moon"
import insert from require "table"

local parse_to_table

call = (name, ...)=>
  args={...}
  insert @_b, {
    _n:name
    _a: [((type(arg) == "function") and (parse_to_table arg, @_G) or arg) for arg in *args]
  }

index = (name)=>
  (...) -> call @, name, ...

parse_to_table = (fn,globals={},args={})->
  buf = {_f:fn}
  env = {_b:buf,_G:globals,_ARR:(tbl)-> _arr:true, unpack [parse_to_table fn,globals for fn in *tbl]}
  mixin_table env, globals
  run_with_scope fn, defaultbl(env, index), unpack args
  buf

parse_to_table
