config = require 'dysnomia.config'

--- caching autolookup table for HTTP methods
-- allows for `method.get` to be resolved to `ngx.HTTP_GET`
methods = setmetatable {}, __index: (name) =>
  with m = ngx['HTTP_' .. name\upper!]
    @[name] = m

--- simple (= no ltn12) request
-- `req` can be a string (url) or a table containing:
-- 
--   * `url`: a string containig the url to request
--   * `body`:
--     * either a string containing the body
--     * or a table that will be urlencoded (in this case the `Content-type` header will be set accordingly)
--   * `headers`: a table containing headers (optional)
--   * `method`: (optional, defaults to `HTTP_POST` if `req.body` exists, `HTTP_GET` if it doesn't)
--     * either a `ngx.HTTP_*` method constant
--     * or a string (e.g. `"get"`)
-- `body` is optional and will override `req.body` (so you don't need a request table if you just want to post to an url with default headers)
simple = (req, body) ->

  if type(req) == "string"
    req = { url: req }

  if body
    req.body = body

  if req.body
    req.method or= ngx.HTTP_POST

  if type(req.body) == "table"
    req.body = encode_query_string req.body
    req.headers or= {}
    req.headers["Content-type"] = "application/x-www-form-urlencoded"

  -- nginx magic (needs trailing /)
  unless req.url\match "//.-/"
    req.url ..= "/"

  if type(req.method)=="string" and methods[req.method]
    req.method = methods[req.method]

  req.method or= ngx.HTTP_GET

  res = ngx.location.capture config.ngx.http.server.request_proxy.location, {
    method: req.method
    body: req.body
    ctx: {
      headers: req.headers
    }
    vars: { [config.ngx.http.server.request_proxy.uri_variable]: req.url }
  }

  res.body, res.status, res.header


{:simple}