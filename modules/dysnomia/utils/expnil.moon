ffi=require'ffi'

EXPNIL_type=ffi.typeof[[enum{dysnomia_utils_expnil}]]

EXPNIL_value = EXPNIL_type!

isexpnil=(value)-> ffi.istype(EXPNIL_type,value)

{expnil:EXPNIL_value,:isexpnil}
