--- Interface for Router classes.
-- 
-- this interface is returned directly by the module
class RouterInterface
  new: => error 'RouterInterface instantiated'

  --- Dispatch an URI request.
  dispatch: (@uri=(ngx and ngx.var.uri))=>
