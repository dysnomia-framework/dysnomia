import insert from require 'table'

config = require 'dysnomia.config'

--- This is how a route handler should behave:
-- 
-- it is called with a list of `matches` (as returned by `ngx.re.match`) and the additional `data` table as arguments
-- 
-- it should return:
-- 
-- * on success: `not nil` (`true` would be a good practice; but you could also use it to return something out of the router's [dispatch](#L.code.dispatch:..uri.ngx.var.uri.data...gt...code.) call)
-- * on error: (in that order)
--   * `nil`,
--   * HTTP error code as a `number`,
--   * a `table` of information for the error handler
_route_handler_t=(matches,data)->
_route_handler_t=nil --getting rid of it again

--- A router implementation using regular expressions.
-- 
-- implements [dysnomia.routers.interface](interface.html)
-- 
-- this class is returned directly by the module
class RegexRouter extends require 'dysnomia.routers.interface'

  --- the internal routing table
  -- (set by constructor)
  routes: nil

  --- the `xpcall` debugger to run on route handler errors if `config.routers.regex_router.debug` is set
  -- (set by constructor)
  debugger: nil

  --- Constructor.
  -- sets [@routes](#L.code.routes:.nil..code.) (default `{}`) and [@debugger](#L.code.debugger:.nil..code.) accordingly.
  new: (@routes={},@debugger=debug.traceback)=>

  --- Add new route.
  -- 
  -- * `pattern` is a regex to match the dispatched uri to,
  -- * `handler` is the [route handler](#L.code._route_handler_t...matches.data.-.gt...code.) function to dispatch to on match
  add: (pattern, handler)=>
    table.insert @routes, {pattern,handler}

  --- Add new route with nested [RegexRouter](#L.code.class.RegexRouter..code.).
  addNested: (pattern,router)=>
    @add router\nested pattern

  --- nest this router.
  -- (usually you don't want to use this directly, but use [RegexRouter\addNested](#L.code.addNested:..pattern.router...gt...code.) instead)
  -- 
  -- this returns a modified pattern and handler that calls the router's [dispatch](#L.code.dispatch:..uri.ngx.var.uri.data...gt...code.) function
  nested: (pattern)=>
    pattern .. '(.*)', (match)->
      @dispatch match[1]
    

  --- dispatch for an URI.
  -- 
  -- this iterates over [@routes](#L.code.routes:.nil..code.) and calls the [route handler](#L.code._route_handler_t...matches.data.-.gt...code.) of the first match.
  -- 
  -- the match table (from `ngx.re.match`) and `data` are supplied as arguments to the dispatched route handler
  -- 
  -- returns:
  -- 
  -- * on success: the return value of the called route handler
  -- * on error: `nil`,`errorcode`,`errordata` (either returned by the route handler or generated as stated below)
  -- 
  -- Error Handling:
  -- 
  -- * on soft error it tries to dispatch the route `__<errorcode>` with `data` set to the 3rd return value of the failed handler.
  -- * if `config.routers.regex_router.debug` enabled: on hard error the errorcode is set to `500` and `data` to the return value of [@debugger](#L.code.debugger:.nil..code.)
  -- * if no match was found the errorcode is set to `404` and `data.uri` to the tried uri
  dispatch: (uri=ngx.var.uri,data={})=>
    super uri
    local err,errdata
    if match=ngx.re.match(@uri,'^__([0-9][0-9][0-9])$','oj')
      err=tonumber match[1]
      errdata=data
    else
      err=404
      errdata={:uri}
    for route in *@routes
      match = ngx.re.match(@uri, '^' .. route[1] .. '$', 'oj')
      if match
        if config.routers and config.routers.regex_router and config.routers.regex_router.debug
          xpret,xperr=xpcall (->
            ret,err,errdata = route[2](match,data)
            return true
          ),@debugger
          if xpret~=true
            err,errdata=500,xperr
            break
        else
          ret,err,errdata = route[2](match,data)
        if ret ~= nil
          return ret
        else
          break
    err or= 500
    return @dispatch '__'..err, errdata if uri ~= '__'..err
    return nil, err, errdata
