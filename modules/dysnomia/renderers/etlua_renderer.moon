import compile from require 'etlua'
import extend from require 'moon'

--- Renderer for etlua templates.
class EtluaRenderer
  --- Constructor.
  -- 
  -- `@args` are the default arguments to the template.
  -- `template` is:
  -- 
  -- * if `str` is `false` (default): the path to an etlua template file (it's recommended to use `loadkit` to locate it)
  -- * if `str` is `true`: an etlua template string,
  new: (template,str=false,@args={})=>
    template = io.open(template,'r')\read'*a' unless str
    @template = compile template

  --- Render the template.
  -- 
  -- any `args` given here override the default ones supplied to the constructor (while falling back to the defaults for any missing here).
  render: (args)=>
    extend args, @args
    @template args
