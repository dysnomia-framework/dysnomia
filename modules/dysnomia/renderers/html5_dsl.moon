dsl=require 'dysnomia.utils.dsl'

import extend from require 'moon'

escape_pattern = do
  punct = "[%^$()%.%[%]*+%-?]"
  (str) ->
    (str\gsub punct, (p) -> "%"..p)
html_escape_entities = {
  ['&']: '&amp;'
  ['<']: '&lt;'
  ['>']: '&gt;'
  ['"']: '&quot;'
  ["'"]: '&#039;'
}
html_escape_pattern = "[" .. table.concat([escape_pattern char for char in pairs html_escape_entities]) .. "]"
escape = (text) ->
  (text\gsub html_escape_pattern, html_escape_entities)
tagtypes={
  void: {'area', 'base', 'br', 'col', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'menuitem', 'meta', 'param', 'source', 'track', 'wbr'}
  rawtext: {'script', 'style'}
  escrawtext: {'textarea', 'title'}
  foreign_containers: {'math','svg'}
}
tagtypes._tag={tag,tagtype for tagtype,list in pairs tagtypes for tag in *list} -- strange syntax (note the for loops being "in reverse order"), unsure if future-proof

--- HTML5 generator from DSL
-- 
-- Example usage:
-- 
-- ```moonscript
-- template=Html5Dsl =>
--   HTML5 ->
--     head ->
--       title @my_page_title
--     body ->
--       h1 'Something'
--       hr!
--       a href:"/foo", class: "bar", ->
--         RAW '&amp;ersand'
-- template.args.my_page_title='Mew!'
-- html_string=template\render!
-- ```
-- 
-- there are 2 special "Elements":
-- 
-- * `HTML5` produces both the `<!DOCTYPE html>` and `html` tag (attributes and children are applied to the latter)
-- * `RAW` receives a string and produces it as-is, without escaping (usually without this strings are escaped)
-- 
-- this class is returned directly by the module
class Html5Dsl
  --- constructor. supply with template dsl function `dslfun` (see above) and optional a table of default `args` (available to `dslfun` via `@...`)
  new: (@dslfun,@args={})=>
  --- this renders the template to a string.
  -- any `args` given here override the default ones supplied to the constructor (while falling back to the defaults for any missing here).
  render: (args={})=>
    extend args, @args
    @@dsl2str dsl(@dslfun,{},{args})
  @dsl2str:(document,foreign=false)=>
    outstr=''
    switch type(document)
      when 'string'
        outstr ..= document
      when 'table'
        for element in *document
          switch type(element)
            when 'string'
              outstr ..= escape element
            when 'table'
              childindex=1
              if element._n=='CDATA'
                outstr ..= escape element._a[1]
              elseif element._n=='RAW'
                outstr ..= element._a[1]
              else
                if element._n=='HTML5'
                  outstr ..= '<!DOCTYPE html><html'
                else
                  outstr ..= '<'..element._n
                is_fcont=(tagtypes._tag[element._n]=='foreign_containers')
                if type(element._a[1])=='table' and (type(element._a[1][1])=='nil' or type(element._a[1]._f)=='nil')
                  outstr ..= ' ' .. table.concat [k..'="'..v..'"' for k,v in pairs element._a[1]],' '
                  childindex=2
                is_fvoid=(is_fcont or foreign or tagtypes._tag[element._n]=='void') and (type(element._a[childindex])=='nil' or (type(element._a[childindex])=='table' and #(element._a[childindex])<1))
                if is_fvoid
                  outstr ..= ' /'
                outstr ..= '>'
                if tagtypes._tag[element._n]~='void'
                  outstr ..= @dsl2str(element._a[childindex],is_fcont or foreign)
                if element._n=='HTML5'
                  outstr ..= '</html>'
                elseif not is_fvoid
                  outstr ..= '</' .. element._n ..'>'
    outstr
