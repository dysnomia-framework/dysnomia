--- Interface for Renderer classes.
-- 
-- this interface is returned directly by the module
class RendererInterface
  new: => error 'RendererInterface instantiated'

  --- Render the template/renderer.
  -- 
  -- `args` are the (optional) arguments to the template/renderer.
  render: (args={})=>
