# dysnomia

[![forthebadge](http://forthebadge.com/images/badges/you-didnt-ask-for-this.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/fuck-it-ship-it.svg)](http://forthebadge.com)

a modular web framework written in moonscript.

[![forthebadge](http://forthebadge.com/images/badges/built-by-codebabes.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/kinda-sfw.svg)](http://forthebadge.com)

## Assisted setup

### first time (project creation)

* get [eris](https://gitlab.com/dysnomia-framework/eris).
* run `eris install /wherever/you/want/to/create/the/instance`
* wait for it to do its magic, specifically it:
  * creates a new folder + git repo for your instance
  * adds dysnomia as a submodule
  * sets it up
  * creates some hello world example files from a template
  * compiles stuff and generates the nginx config
  * commits everything for you
* from your instance's directory:
  * run `eris rebuild` to build the instance's code
  * run `eris run` to start the server
* navigate to http://127.0.0.1:8080/
* whenever you change the moonscript code or config, remember to run `eris rebuild` (to compile stuff) and `eris control reload` (to make nginx reload stuff; alternatively just restart it)

### project clone or after submodule upgrades

* get [eris](https://gitlab.com/dysnomia-framework/eris).
* from your instance directory:
  * run `eris install` to set up dysnomia
  * run `eris rebuild` to build your instance's code
  * run `eris run` to start the server

## Manual setup

### Installation

You can install all the dependencies you need to use dysnomia by simply running `./setup`. you shouldn't yet, though, as this is in very early development, and we're currently thinking.

### Testing

* create a folder `$APP` for your application
* install dysnomia to a folder `$DYS`
* create a debug config `$APP/config.moon`

```moonscript
->
  app ->
    debug true
  ngx ->
    debug true
    http ->
      server ->
        port 8088
```

* create an example `$APP/init.moon`

```moonscript
RegexRouter = require 'dysnomia.routers.regex_router'
HDSL= require 'dysnomia.renderers.html5_dsl'

config = require 'dysnomia.config'

subApp= with RegexRouter!
  \add '/', ->
    ngx.say 'Hello, SubApp!'
    ngx.exit(ngx.OK)

template1=HDSL =>
  HTML5 ->
    head ->
      title 'Hello World!'
    body ->
      if @matches[1]
        p @matches[1]
      else
        p 'Here be dragons'

with RegexRouter!
  \add '/', ->
    ngx.say 'Hello, World!'
    ngx.exit(ngx.OK)
  \addNested '/sub', subApp
  \add '/method', ->
    ngx.say ngx.req.get_method!
    ngx.exit(ngx.OK)
  \add '/error', ->
    error 'simulated error'
    return true
  \add '/(.*)', (matches)->
    ngx.say template1\render {:matches}
    ngx.exit(ngx.OK)
  \add '__404', (matches,data)->
    ngx.status = 404
    ngx.say '404 not found: '..data.uri
    if config.app.debug
      ngx.say '<code><pre>'
      ngx.say debug.traceback!
      ngx.say '</pre></code>'
    ngx.exit(ngx.OK)
  \add '__500', (matches,data)->
    ngx.status = 500
    ngx.say '<code><pre>'
    ngx.say data
    ngx.say '</pre></code>'
    ngx.exit(ngx.OK)
  \dispatch!
```

* compile: `$DYS/.run moonc $APP`
* generate `nginx.conf`: `$DYS/tools/gen_ngx_config` (while `$PWD==$APP`)
* run server: `$DYS/.run nginx -p $APP -c nginx.conf`
* navigate to http://127.0.0.1:8088/
